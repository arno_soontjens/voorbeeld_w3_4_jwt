import { MSG_NO_USER_FOUND } from "../util/errorHander";
import { hashPassword } from "./util";

const users = [];

function removeHashedPassword(user) {
  delete user.hashed_password;
  return user;
}

export async function findUserById(id) {
  // Promise wordt enkel en alleen gebruikt omdat een echte database call ook
  // een promise zou returnen.
  return new Promise((resolve, reject) => {
    try {
      if (id === undefined) throw new Error("The id cannot be undefined");
      const found = users.find((user) => user.id === id);
      if (!found) throw new Error(`A user with ID ${id} does not exist.`);
      resolve(removeHashedPassword(found));
    } catch (error) {
      reject(error);
    }
  });
}

async function _findUserByUsername(username, withPassword = false) {
  // deze Promise wordt hier enkel en alleen gebruikt omdat een echte database call ook
  // een promise zou returnen, ze heeft geen andere functie dan een educatieve (want er komen in de body van de functie geen promises voor).
  return new Promise((resolve, reject) => {
    try {
      if (!username) throw new Error("The username cannot be undefined.");
      const found = users.find(
        (user) => user.username === username.toLowerCase()
      );
      if (!found) throw new Error(MSG_NO_USER_FOUND);
      resolve(withPassword ? found : removeHashedPassword(found));
    } catch (error) {
      reject(error);
    }
  });
}

export const findUserByUsername = async (username) =>
  _findUserByUsername(username);
export const findUserByUsernameWithPassword = async (username) =>
  _findUserByUsername(username, true);

export async function insertNewUser({
  username,
  // Dit is het 'plain text' wachtwoord. Deze functie zorgt voor de hash
  password,
  firstName,
  lastName,
  address,
  phone,
}) {
  const hashed = await hashPassword(password);
  users.push({
    id: users.length,
    username: username.toLowerCase(),
    hashed_password: hashed,
    firstName,
    lastName,
    address,
    phone,
  });
  //console.log("Inserted new user:", users[users.length - 1]);
  return users[users.length - 1];
}

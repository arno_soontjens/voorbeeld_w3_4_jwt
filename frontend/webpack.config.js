const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");

const dist = path.resolve(__dirname, "dist");

module.exports = {
  devtool: "source-map",
  mode: "development",
  entry: "./src/index.js",
  module: {
    rules: [
      // HTML loader
      {
        test: /\.html$/i,
        loader: "html-loader",
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      // Image assets
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: "asset/resource",
      },
      // Font assets
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: "asset/resource",
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "src/html/index.html"),
    }),
  ],
  devServer: {
    contentBase: dist,
    open: true,
    compress: true,
    overlay: true,
  },
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "dist"),
    clean: true,
  },
};

import "bootstrap/dist/css/bootstrap.min.css";
import "./css/style.css";

import loginPage from "./html/login.html";
import mainPage from "./html/home.html";
import { getTokenFromStorage } from "./js/util";
import { addLogoutHandler, getLoggedInUser } from "./js/home";
import { addLoginHandler } from "./js/login";

const pageContent = document.getElementById("content");

function loadPageContent() {
  getTokenFromStorage().then((token) => {
    if (!token) {
      // unauthenticated.
      pageContent.innerHTML = loginPage;
      addLoginHandler();
    } else {
      // Authenticated
      pageContent.innerHTML = mainPage;
      addLogoutHandler();
      getLoggedInUser().then((data) => {
        document.getElementById("user-data").innerHTML = data.address;
      });
    }
  });
}
export const refreshContent = () => loadPageContent();

loadPageContent();

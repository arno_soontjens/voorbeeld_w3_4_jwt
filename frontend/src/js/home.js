import { refreshContent } from "..";
import { API_URL } from "./login";
import { removeTokenFromStorage, getTokenFromStorage } from "./util";

export function getLoggedInUser() {
  return getTokenFromStorage().then((token) => {
    return fetch(API_URL + "/users/me", {
      headers: {
        Authorization: "Bearer " + token,
      },
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error(response.statusText);
        }
        return response.json();
      })
      .catch((error) => {
        console.error(error);
        throw error;
      });
  });
}

export function addLogoutHandler() {
  const logoutButton = document.getElementById("logout");
  logoutButton.addEventListener("click", (event) => {
    event.preventDefault();
    logout();
  });
}

export function logout() {
  removeTokenFromStorage().then(() => refreshContent());
}

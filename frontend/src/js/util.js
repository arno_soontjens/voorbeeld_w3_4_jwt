import localforage from "localforage";

const KEY_ACCESS_TOKEN = "access_token";

export function persistTokenToStorage(token) {
  return localforage.setItem(KEY_ACCESS_TOKEN, token);
}

export function removeTokenFromStorage() {
  return localforage.removeItem(KEY_ACCESS_TOKEN);
}

export function getTokenFromStorage() {
  return localforage.getItem(KEY_ACCESS_TOKEN);
}

import { refreshContent } from "..";
import { persistTokenToStorage } from "./util";

export const API_URL = "http://localhost:8000";

export function getCredentialsFromForm() {
  const usernameInput = document.getElementById("email-input");
  const passwordInput = document.getElementById("password-input");
  return { username: usernameInput.value, password: passwordInput.value };
}

export function showLoginError(errorMessage) {
  const errorFeedback = document.getElementById("login-error");
  errorFeedback.innerHTML = errorMessage;
  errorFeedback.style.display = "block";
}

export function login(username, password) {
  return fetch(API_URL + "/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ username, password }),
  })
    .then((response) => {
      if (!response.ok) throw new Error(response.statusText);
      return response.json();
    })
    .catch((error) => {
      console.error("Could not login: ", error);
      throw error;
    });
}

function storeTokenAndRefresh(token) {
  if (token !== undefined) {
    persistTokenToStorage(token).then(() => {
      console.log("Token persisted to storage!");
      refreshContent();
    });
  }
}

export function addLoginHandler() {
  const loginButton = document.getElementById("btn_login");
  loginButton.addEventListener("click", (event) => {
    event.preventDefault();
    const cred = getCredentialsFromForm();
    login(cred.username, cred.password)
      .then((result) => {
        storeTokenAndRefresh(result.token);
      })
      .catch((error) => showLoginError(error));
  });
}
